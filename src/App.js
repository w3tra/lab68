import React, {Component, Fragment} from 'react';
import TodoApp from "./containers/TodoApp/TodoApp";

class App extends Component {
  render() {
    return (
     <Fragment>
       <TodoApp/>
     </Fragment>
    );
  }
}

export default App;
