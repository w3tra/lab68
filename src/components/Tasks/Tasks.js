import React from 'react';
import './Tasks.css';
import {Tooltip} from "@material-ui/core";

const Tasks = (props) => {
  return(
    <div className='task-desk'>
      <div className={props.isChecked ? 'task checked' : 'task'}>
        {props.text}
        <Tooltip title="delete task">
          <i className="material-icons removeBtn" onClick={props.removed}>delete</i>
        </Tooltip>
        <Tooltip title="task done">
          <input className="task-checkbox" type="checkbox" onClick={props.checked}/>
        </Tooltip>
      </div>
    </div>
  )
};

export default Tasks;