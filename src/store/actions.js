import axios from 'axios';
export const INPUT_CHANGE = 'INPUT_CHANGE';
export const FETCH_TASK_REQUEST = "FETCH_TASK_REQUEST";
export const FETCH_TASK_SUCCESS = "FETCH_TASK_SUCCESS";
export const FETCH_TASK_ERROR = "FETCH_TASK_ERROR";
export const CHECKED = 'CHECKED';
export const STOP_LOADING = 'STOP_LOADING';

export const currentTaskChange = (value) => {
  return {type: INPUT_CHANGE, value};
};

export const removeTask = (id) => {
  return dispatch => {
    axios.delete(`/todo/${id}.json`).then(() => dispatch(getTask()));
  }
};

export const createTask = () => {
  return (dispatch, getState) => {
    const task = getState().currentTask;
    axios.post('/todo.json', {task: task, checked: false}).then(() => {
      dispatch(getTask());
    });
  };
};

export const getTask = () => {
  return dispatch => {
    dispatch(fetchTaskRequest());
    axios.get('/todo.json').then(response => {
      response.data
        ? dispatch(fetchTaskSuccess(response.data))
        : dispatch(stopLoadingHandler());
    }, error => dispatch(fetchTaskError(error)))
  }
};

export const stopLoadingHandler = () => {
  return {type: STOP_LOADING};
};

export const fetchTaskRequest = () => {
  return { type: FETCH_TASK_REQUEST };
};

export const fetchTaskSuccess = (data) => {
  return dispatch => {
    dispatch({type: FETCH_TASK_SUCCESS, data});
  };
};

export const fetchTaskError = (error) => {
  return { type: FETCH_TASK_ERROR, error };
};

export const checkedTaskHandler = (id) => {
  return {type: CHECKED, id};
};